"""
This file contains incomplete versions of some agents that can be selected to control Pacman.
You will complete their implementations.

To select an agent, use the '-p' option when running pacman.py.
Arguments can be passed to your agent using '-a'.
For example, to load a SearchAgent that uses depth first search (dfs), run the following command:

> python pacman.py -p SearchAgent -a searchFunction=depthFirstSearch

Commands to invoke other search strategies can be found in the project description.

Please only change the parts of the file you are asked to.
Look for the lines that say:
"*** Your Code Here ***"

Good luck and happy searching!
"""

import game
import search
import searchAgents
import search_student
import util
from util import manhattanDistance as mhd
import itertools

class CornersProblem(search.SearchProblem):
    """
    This search problem finds paths through all four corners of a layout.

    You must select a suitable state space and successor function.

    See the searchAgents.PositionSearchProblem class for an example of
    a working SearchProblem.
    """

    def __init__(self, startingGameState):
        """
        Stores the walls, pacman's starting position and corners.
        """

        self.walls = startingGameState.getWalls()
        self.startingPosition = startingGameState.getPacmanPosition()
        top = self.walls.height - 2
        right = self.walls.width - 2

        self.corners = ((1, 1), (1, top), (right, 1), (right, top))
        for corner in self.corners:
            if not startingGameState.hasFood(*corner):
                print('Warning: no food in corner ' + str(corner))

        self._expanded = 0  # Number of search nodes expanded

    def startingState(self):
        """
        Returns the start state (in your state space, not the full Pacman state space)
        """
        c_visited = [False,False,False,False]
        start = self.startingPosition
        for pos in self.corners:
            if start == pos:
                c_visited[self.corners.index(pos)] = True
            
        return (start, tuple(c_visited))
    
    def isGoal(self, state):
        """
        Returns whether this search state is a goal state of the problem
        """
        pos, c_visited = state
        for visited in c_visited:
            if visited == False:
                return False
        return True
    def successorStates(self, state):
        """
        Returns successor states, the actions they require, and a cost of 1.

        As noted in search.py:
        For a given state, this should return a list of triples, (successor, action, stepCost),
        where 'successor' is a successor to the current state, 'action' is the action
        required to get there, and 'stepCost' is the incremental
        cost of expanding to that successor"""
        
        currentPosition, c_visited = state
        #print(currentPosition)
        successors = []
        for action in [game.Directions.NORTH, game.Directions.SOUTH, game.Directions.EAST, game.Directions.WEST]:
            # Add a successor state to the successor list if the action is legal
            # Here's a code snippet for figuring out whether a new position hits a wall:
            x, y = currentPosition
            #print("x,y", x,y)
            dx, dy = game.Actions.directionToVector(action)
            nextx, nexty = int(x + dx), int(y + dy)
            #print("nextx, nexty", nextx,nexty)
            hitsWall = self.walls[nextx][nexty]
            if not hitsWall:
                temp_cv = list(c_visited)
                if (nextx,nexty) in self.corners:
                    temp_cv[self.corners.index((nextx,nexty))] = True
                    successors.append((((nextx,nexty),tuple(temp_cv)), action, 1))
                else:
                    successors.append((((nextx,nexty),c_visited), action, 1))    
        self._expanded += 1
        return successors
    
    def actionsCost(self, actions):
        """
        Returns the cost of a particular sequence of actions.  If those actions
        include an illegal move, return 999999.  This is implemented for you.
        """

        if actions == None:
            return 999999

        x, y = self.startingPosition
        for action in actions:
            dx, dy = game.Actions.directionToVector(action)
            x, y = int(x + dx), int(y + dy)
            if self.walls[x][y]:
                return 999999

        return len(actions)

def cornersHeuristic(state, problem):
    """
    A heuristic for the CornersProblem that you defined.

        state:   The current search state (a data structure you chose in your search problem)

        problem: The CornersProblem instance for this layout.

    This function should always return a number that is a lower bound
    on the shortest path from the state to a goal of the problem; i.e.
    it should be admissible.  (You need not worry about consistency for
    this heuristic to receive full credit.)
    """

    corners = problem.corners 
    walls = problem.walls 
    current, visited = state
    #keep track of corners that have not yet been reached
    unvisited = []
    for idx,corner in enumerate(problem.corners):
        if visited[idx] is False:
            unvisited.append(corner)
    #if we have reached all of them, estimate distance to be 0
    if not unvisited:
        return 0
    #pick the first corner in the list that has not yet been visited      
    selected = unvisited[0]
    #estimate the remaining distance to be 0 so far
    total_distance = 0
    #estimate the path to be the combined smallest manhattan distance between the remaining corners
    #corners will be removed from unvisited as their closest counterpart is found
    while unvisited:
        distances = []
        #for each corner remaining that is NOT the initially selected corner,
        for corner in unvisited:
            if corner != selected:
                #check how far it is from all other unvisited corners
                distances.append((mhd(selected, corner), corner))
            #If the selected corner is the only remaining corner, there is no more distance to travel
            elif len(unvisited) == 1:
                distances.append((0,corner))
        #find the closest corner to the selected corner
        temp = min(distances, key = lambda t: t[0])
        #add the distance to the running count
        total_distance += temp[0]
        #remove the selected corner from the list, its closest counterpart has already been found
        unvisited.remove(selected)
        #reassign the selected corner to be the one that was found closest to the previous corner
        selected = temp[1]
    #return this estimated distance
    return total_distance
    
#Method for finding the closest corner in remainingCorners to the position in currentLocation
def closestCorner(currentLocation, remainingCorners):
    initial_distances = []
    for corner in remainingCorners:
        initial_distances.append((mhd(currentLocation,corner),corner))
    return min(initial_distances, key = lambda t: t[0])[1]
    
        
def foodHeuristic(state, problem):
    """
    Your heuristic for the FoodSearchProblem goes here.

    This heuristic must be consistent to ensure correctness.  First, try to come up
    with an admissible heuristic; almost all admissible heuristics will be consistent
    as well.

    If using A* ever finds a solution that is worse uniform cost search finds,
    your heuristic is *not* consistent, and probably not admissible!  On the other hand,
    inadmissible or inconsistent heuristics may find optimal solutions, so be careful.

    The state is a tuple (pacmanPosition, foodGrid) where foodGrid is a
    Grid (see game.py) of either True or False. You can call foodGrid.asList()
    to get a list of food coordinates instead.

    If you want access to info like walls, capsules, etc., you can query the problem.
    For example, problem.walls gives you a Grid of where the walls are.

    If you want to *store* information to be reused in other calls to the heuristic,
    there is a dictionary called problem.heuristicInfo that you can use. For example,
    if you only want to count the walls once and store that value, try:
        problem.heuristicInfo['wallCount'] = problem.walls.count()
    Subsequent calls to this heuristic can access problem.heuristicInfo['wallCount']
    """
    position, foodGrid = state
    food_list = foodGrid.asList()
    distances = []
    #the manhattan distance from the farthest food object is used to estimate the distance remaining for a solution.
    if len(food_list)==0:
        return 0
    for food in food_list:
        distances.append(mhd(position,food))
    return max(distances)
    


class ClosestDotSearchAgent(searchAgents.SearchAgent):
    """
    Search for all food using a sequence of searches
    """

    def registerInitialState(self, state):
        self.actions = []
        currentState = state

        while (currentState.getFood().count() > 0):
            nextPathSegment = self.findPathToClosestDot(currentState)  # The missing piece
            self.actions += nextPathSegment

            for action in nextPathSegment:
                legal = currentState.getLegalActions()
                if action not in legal:
                    raise Exception('findPathToClosestDot returned an illegal move: %s!\n%s' % (str(action), str(currentState)))

                currentState = currentState.generateSuccessor(0, action)

        self.actionIndex = 0
        print('Path found with cost %d.' % len(self.actions))

    def findPathToClosestDot(self, gameState):
        """
        Returns a path (a list of actions) to the closest dot, starting from gameState
        """

        # Here are some useful elements of the startState
        startPosition = gameState.getPacmanPosition()
        food = gameState.getFood()
        walls = gameState.getWalls()
        problem = AnyFoodSearchProblem(gameState)

        # *** Your Code Here ***
        #watching pacman try this using DFS and UCS was entertaining, BFS produces by far the best solution.
        return search_student.breadthFirstSearch(problem)

class AnyFoodSearchProblem(searchAgents.PositionSearchProblem):
    """
    A search problem for finding a path to any food.

    This search problem is just like the PositionSearchProblem, but
    has a different goal test, which you need to fill in below.  The
    state space and successor function do not need to be changed.

    The class definition above, AnyFoodSearchProblem(searchAgents.PositionSearchProblem),
    inherits the methods of the PositionSearchProblem.

    You can use this search problem to help you fill in
    the findPathToClosestDot method.
    """

    def __init__(self, gameState):
        """
        Stores information from the gameState.  You don't need to change this.
        """

        # Store the food for later reference
        self.food = gameState.getFood()
        self.food_list = self.food.asList()
        # Store info for the PositionSearchProblem (no need to change this)
        self.walls = gameState.getWalls()
        self.startState = gameState.getPacmanPosition()
        self.costFn = lambda x: 1
        self._visited = {}
        self._visitedlist = []
        self._expanded = 0

    def isGoal(self, state):
        """
        The state is Pacman's position. Fill this in with a goal test
        that will complete the problem definition.
        """

        x, y = state

        # *** Your Code Here ***
        #is the tile pacman is on a food tile? if yes, great! if not, not great.
        return (x,y) in self.food_list

class ApproximateSearchAgent(game.Agent):
    """
    Implement your contest entry here.  Change anything but the class name.
    """

    def registerInitialState(self, state):
        """
        This method is called before any moves are made.
        """

        # *** Your Code Here ***
        util.raiseNotDefined()

    def getAction(self, state):
        """
        From game.py:
        The game.Agent will receive a GameState and must return an action from
        game.Directions.{North, South, East, West, Stop}
        """

        # *** Your Code Here ***
        util.raiseNotDefined()
        return None
